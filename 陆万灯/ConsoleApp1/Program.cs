﻿using System;
using System.Collections;

namespace ConsoleApp1
{
    class Program
    {
        static void Main(string[] args)
        {
            SortedList sortedList = new SortedList();
            sortedList.Add(1,"蒸羊羔");
            sortedList.Add(2,"烧花鸭");
            sortedList.Add(3,"烧子鹅");
            sortedList.Add(4,"烤全羊");
            Console.WriteLine("请输入你要查询的菜品：");
            int id = int.Parse(Console.ReadLine());
            bool flag = sortedList.ContainsKey(id);
            if (flag)
            {
                string name = sortedList[id].ToString();
                Console.WriteLine("你要查询的菜品为：{0}",name);
            }
            else
            {
                Console.WriteLine("你输入的菜品不存在！");
            }
            Console.WriteLine("所有菜品信息如下：");
            foreach(DictionaryEntry d in sortedList)
            {
                int key =(int)d.Key;
                string value = d.Value.ToString();
                Console.WriteLine("菜品序号：{0}\n菜品名称：{1}",key,value);
            }
        }
    }
}
