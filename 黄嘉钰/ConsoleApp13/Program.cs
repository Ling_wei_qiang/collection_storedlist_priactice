﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp13
{
    class Program
    {
        static void Main(string[] args)
        {
            SortedList sl = new SortedList();
            sl.Add(1, "《大逃脱》");
            sl.Add(2, "《新西游记》");
            sl.Add(3, "《青春有你2》");
            sl.Add(4, "《HIT THE ROAD》");
            sl.Add(5, "《Plan：D vlog》");

            while (true)
            {
                Console.WriteLine("\r\n   ======   请选择操作   ======");
                Console.WriteLine("   1.收藏综艺     2.查找     3.ALL");
                Console.WriteLine("==================================\r\n");
                Console.WriteLine("请输入你的选择：");
                string num = Console.ReadLine();
                switch (num)
                {
                    case "1":
                        Console.WriteLine("请输入综艺编号：");
                        int id =int.Parse( Console.ReadLine());
                        Console.WriteLine("请输入综艺名称：");
                        string name = Console.ReadLine();
                        if (sl.ContainsKey(id))
                        {
                            Console.WriteLine("该综艺已收藏");
                            return;
                        }
                        break;
                    case "2":
                        Console.WriteLine("请输入综艺编号：");
                        int idFind = int.Parse(Console.ReadLine());
                        object nameFind =sl[idFind];
                        if (nameFind == null)
                        {
                            Console.WriteLine("该编号不存在");
                        }
                        else
                        {
                            Console.WriteLine($"你所查找的综艺名称是：{nameFind.ToString()}");
                        }
                        break;
                    case "3":
                        Console.WriteLine("所有的综艺信息：");
                        foreach (DictionaryEntry item in sl)
                        {
                            object key = item.Key;
                            object value = item.Value;
                            Console.WriteLine("综艺编号：{0}，综艺名称：{1}", key, value);
                        }
                        Console.WriteLine($"************共有{sl.Count}部综艺************");
                        break;
                }

            }
        }
    }
}
