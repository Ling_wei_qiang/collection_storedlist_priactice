﻿using System;

namespace ConsoleApp1
{
    class MyTest<T>
    {
        private T[] items = new T[5];
        private int index = 0;
        //向数组中添加项
        public void Add(T t)
        {
            if (index < 4)
            {
                items[index] = t;
                index++;
            }
            else
            {
                Console.WriteLine("数组已满了！");
            }
        }
        //读取数组中的全部项
        public void Show()
        {
            foreach (T t in items)
            {
                Console.WriteLine(t);
            }
        }
    }
}