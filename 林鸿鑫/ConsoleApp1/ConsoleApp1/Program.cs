﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1
{
    class Program
    {
        static void Main(string[] args)
        {
            SortedList list = new SortedList();

            list.Add(9, "《绝对的忠诚》");
            list.Add(55, "《中华人民共和国》");
            list.Add(2, "《战狼4》");

            Console.WriteLine("请输入你的所需要的电影的序号：");

            int id = int.Parse(Console.ReadLine());

            bool ture = list.ContainsKey(id);

            if (ture)
            {
                String name = list[id].ToString();
                Console.WriteLine("你所查找的电影的名字：{0}", name);


            }
            else
            {
                Console.WriteLine("你所查找的电影找不到啊，我也不知道你在搞啥子！！！");
            }
            Console.WriteLine("以下是所有的电影的序号和名字：");
            foreach (DictionaryEntry all in list)
            {
                int key = (int)all.Key;
                string value = all.Value.ToString();
                Console.WriteLine("电影序号:{0},电影名字:{1}",key,value );
            }
            Console.WriteLine();
            Console.WriteLine();
            //泛型
            //在使用的时候一定要两个参数类型要相同，不能就会输出错误！

            Add<double >(3,36.66);

            Add<double>(4,87.2);
             

             void  Add<T> (T a, T b)
            {
                double AUG = (double.Parse(a.ToString()) + double.Parse(b.ToString()))/2;
                Console.WriteLine(AUG );

            }


            Console.WriteLine();
            Console.WriteLine( );

            //泛型二

            MyTest<int> test = new MyTest<int>();
            test.Add(10);
            test.Add(20);
            test.Add(30);
            test.Show();


        }


    }
}
