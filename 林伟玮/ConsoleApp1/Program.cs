﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1
{
    class Program
    {
        static void Main(string[] args)
        {
            SortedList bus = new SortedList();
            bus.Add (1,"550");
            bus.Add(2, "538");
            bus.Add(3, "643");
            bus.Add(4, "644");

            Console.WriteLine("请输入你的号数：");
            int id = int.Parse(Console.ReadLine());
            bool resd = bus.ContainsKey(id);  //判断是否包含特定值

            if (resd  )
            {
                Console.WriteLine("你的总分为：{0}", bus[id].ToString());
            }
            else
            {
                Console.WriteLine("输入错误哦~");
            }

            Console.WriteLine("所有人的成绩都在这了。");
            foreach (DictionaryEntry d in bus)    //遍历
            {
                int key = (int)d.Key;
                string value = d.Value.ToString();
                Console.WriteLine("号数为{0}，总分为{1}", key, value);
            }


        }
    }



}
