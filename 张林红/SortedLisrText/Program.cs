﻿using System;
using System.Collections;
using System.Collections.Generic;

namespace SortedListText
{
    class Program
    {
        static void Main(string[] args)
        {
            SortedList so = new SortedList();
            so.Add(1, "甜点");
            so.Add(2, "奶茶");
            so.Add(3, "汽水");
            so.Add(4, "零食");
            so[5] ="矿泉水";

            Console.WriteLine("请输入你想要的食物");
            int id = int.Parse(Console.ReadLine());
            bool food = so.ContainsKey(id);
            if (food)
            {
                string foodName = so[id].ToString();
                Console.WriteLine("你所选择的是:{0}",foodName);
            }
            else
            {
                Console.WriteLine("该商品售空");
            }


            foreach (DictionaryEntry v in so)
            {
                int key = (int)v.Key;
                string value = v.Value.ToString();
                Console.WriteLine("食物编号：{0}，食物名：{1}", key, value);
            }
            

        }
    }
}
