﻿using System;
using System.Collections;

namespace ConsoleApp22
{
    class Program
    {
        static void Main(string[] args)
        {
            SortedList list = new SortedList();
            list.Add(2,"雨里来");
            list.Add(4,"夏而归");
            list.Add(3,"冬而离");
            list.Add(1,"风里去");

            foreach (DictionaryEntry  item in list)
            {
                Console.WriteLine("{0}  {1}",item.Key,item.Value );
            }
        }
    }
}
