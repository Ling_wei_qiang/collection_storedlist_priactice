﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Net.Http.Headers;

namespace ConsoleApp1
{
    class Program
    {
        static void Main(string[] args)
        {
            SortedList sortedlist = new SortedList();

            sortedlist.Add(99, "超群");

            sortedlist.Add(66, "海水");

            sortedlist.Add(90, "沫城");

            foreach(DictionaryEntry item in sortedlist)
            {
                Console.WriteLine("{0}{1}",item.Key,item.Value);
            }
            SortedList<int, string> newsortedlist = new SortedList<int, string>();

            newsortedlist.Add(66, "我是你");

            newsortedlist.Add(33, "我是他");

            newsortedlist.Add(55, "他是你");

            foreach (var newitem in newsortedlist)
            {
                Console.WriteLine(newitem);
            }

        }
    }
}
