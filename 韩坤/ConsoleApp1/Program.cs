﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1
{
    class Program
    {
        static void Main(string[] args)
        {
            SortedList sl = new SortedList();
            sl.Add(3, "张三");
            sl.Add(1, "李四");
            sl.Add(2, "王五");

            foreach (DictionaryEntry item in sl)
            {
                Console.WriteLine("学号:{0} 姓名:{1}",item.Key,item.Value);
            }
        }
    }
}
