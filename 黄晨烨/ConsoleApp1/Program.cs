﻿using System;
using System.Collections;

namespace ConsoleApp1
{
    class Program
    {
        static void Main(string[] args)
        {
            SortedList sortedList = new SortedList();
            sortedList.Add(1, "旺旺脆冰冰");
            sortedList.Add(2, "可口可乐");
            sortedList.Add(3, "黑糖麻花");
            sortedList.Add(4, "大白兔奶糖");
            Console.WriteLine("Let's go shopping!!");
            Console.WriteLine("以下为开学小卖部仅有的零食，挑选你所想购买的");
            foreach (DictionaryEntry d in sortedList)
            {
                int key = (int)d.Key;
                string value = (string)d.Value;

                Console.WriteLine("零食编码：{0}   零食名称：{1}" ,key,value);
            }
            int id = int.Parse(Console.ReadLine());
            bool flag = sortedList.ContainsKey(id);
            if (flag)
            {
                string name = sortedList[id].ToString();
                Console.WriteLine(name + "已加入购物车");
            }
            else { 
               Console.WriteLine("库存为零");
            }
         



        }
    }
}
