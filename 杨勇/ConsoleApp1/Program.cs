﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1
{
    class Program
    {
        static void Main(string[] args)
        {
            //sortedlist自动排序
            SortedList sorted = new SortedList();
            sorted.Add(1, "海风");
            sorted.Add(3, "凉薄");
            sorted.Add(2, "里阿布吉");
            sorted.Add(5, "额尔古纳");
            foreach (DictionaryEntry item in sorted)
            {
                int key = (int)item.Key;
                string value = (string)item.Value;
                Console.WriteLine("编号{0},署名{1}",key,value);
            }
            Hashtable hashtable = new Hashtable();
            hashtable.Add(3, "少年");
            hashtable.Add(1, "不是因为年轻");
            hashtable.Add(2, "因为理想");
            foreach (DictionaryEntry item in hashtable)
            {
                int key = (int)item.Key;
                string value = (string)item.Value;
                Console.WriteLine("编号{0},署名{1}", key, value);
            }
            //sortedlist小到大，hashtable大到小
            int? num1 = null;
            int? num2 = 0;
            if (num1.HasValue ||num2.HasValue)
            {
                Console.WriteLine("num1,num2都不为空");
            }
            else if(num1.HasValue && num2.HasValue)
            {
                Console.WriteLine("num1,num2有一个为空");
            }
            else
            {
                Console.WriteLine("都不为空");
            }
        }
    }
}
