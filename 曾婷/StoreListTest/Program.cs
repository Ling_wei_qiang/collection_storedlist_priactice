﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SortedListTest
{
    class Program
    {
        static void Main(string[] args)
        {
            SortedList<string,int> student = new SortedList<string,int>();
            
            student.Add("张铭",78);
            student.Add("李恩", 85);
            student.Add("陈达", 49);
            student.Add("黄欢", 60);
            Console.WriteLine("请输入学生姓名：");
            string name = Console.ReadLine();
            bool flag = student.ContainsKey(name);
            if (flag)
            {
                int score = student[name];
                Console.WriteLine("您查找的学生成绩为：{0}",score);
            }
            else
            {
                Console.WriteLine("您查找的学生不在这个学校");
            }
            Console.WriteLine("所有学生的成绩如下");

            foreach(var v in student)
            {
                string key = (string)v.Key;
                int value = v.Value;
                Console.WriteLine("学生姓名：{0}   成绩：{1}",key,value);
            }
        }
    }
}
