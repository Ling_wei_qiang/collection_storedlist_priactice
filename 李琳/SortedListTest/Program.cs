﻿using System;
using System.Collections;

namespace SortedListTest
{
    class Program
    {
        static void Main(string[] args)
        {
            SortedList sortedList = new SortedList();
            sortedList.Add(1, "《活着》");
            sortedList.Add(2, "《人间失格》");
            sortedList.Add(3, "《橘子，不是唯一的水果》");

            Console.WriteLine("请输入图书编号:");
            int id = int.Parse(Console.ReadLine());
            bool flag = sortedList.ContainsKey(id);

            if (flag)
            {
                string name = sortedList[id].ToString();
                Console.WriteLine("您查找的书籍为:{0}", name);
            }
            else
            {
                Console.WriteLine("您查找的书籍已下架！");
            }
            Console.WriteLine("所有书籍信息如下:");        

            foreach(DictionaryEntry a in sortedList)
            {
                int Key = (int)a.Key;
                string value = a.Value.ToString();
                Console.WriteLine("书籍编号:{0},书名:{1}", Key, value);
            }


        }
    }
}
