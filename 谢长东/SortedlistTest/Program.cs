﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SortedlistTest
{
    class Program
    {
        static void Main(string[] args)
        {
            //泛型
            SortedList<int,string> sortedList = new SortedList<int,string>();

            sortedList.Add(401, "李某某");
            sortedList.Add(402, "张某某");
            sortedList.Add(403, "谢某某");
            sortedList.Add(404, "李华");
            sortedList.Add(405, "翠华");


            IEnumerator ienum=sortedList.GetEnumerator();

            //遍历集合（一）
            Console.WriteLine("获取集合全部的内容");
            while (ienum.MoveNext())
            {
                Console.WriteLine(ienum.Current);
            }

            Console.WriteLine("--------");

            //遍历集合（二）
            foreach (KeyValuePair<int,string> item in sortedList)
            {
                Console.Write("房间号为："+item.Key);
                Console.WriteLine("  宿舍大佬为：{0}",item.Value);
            }







            //非泛型
            SortedList sortedList1 = new SortedList();


            sortedList1.Add(401, "李某某");
            sortedList1.Add(402, "张某某");
            sortedList1.Add(403, "谢某某");

            IDictionaryEnumerator idction= sortedList1.GetEnumerator();

            while (idction.MoveNext())
            {
                Console.Write("房间号：{0}", idction.Key);
                Console.WriteLine("  宿舍大佬：{0}", idction.Value);
            }

            Console.WriteLine("--------");

            foreach (DictionaryEntry item in sortedList1)
            {
                Console.Write("房间号为：" + item.Key);
                Console.WriteLine("  宿舍大佬为：{0}", item.Value);
            }

            Console.ReadKey();
        }


    }
}
