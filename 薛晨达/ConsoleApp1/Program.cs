﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1
{
    class Program
    {
        static void Main(string[] args)
        {
            SortedList sortList = new SortedList();
            sortList.Add("B", "呗呗");
            sortList.Add("D", "嘟嘟");
            sortList.Add("C", "初夏");
            sortList.Add("A", "阿晨");
            sortList.Add("E", "恩佐");


            Console.WriteLine("请输入用户编号：");
            string id = Console.ReadLine();
            bool flag = sortList.ContainsKey(id);

            if (flag)
            {
                Console.WriteLine("您查找的用户名为：{0}", sortList[id].ToString());
            }
            else
            {
                Console.WriteLine("您查找的用户名不存在！");
            }

            Console.WriteLine("所有的用户名如下：");

            foreach (DictionaryEntry d in sortList)
            {
                string key = (string)d.Key;
                string value = d.Value.ToString();
                Console.WriteLine("用户编号：{0}，用户名称：{1}", key, value);
            }
        }
    }
}
