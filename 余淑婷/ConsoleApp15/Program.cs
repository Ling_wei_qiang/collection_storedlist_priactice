﻿using System;
using System.Collections;

namespace ConsoleApp15
{
    class Program
    {
        static void Main(string[] args)
        {
            SortedList list = new SortedList();
            list.Add(1, "肯德基");
            list.Add(2, "麦当劳");
            list.Add(3, "塔斯汀");
            list.Add(4, "华莱士");
            Console.WriteLine("输入想吃的品牌的编号：");
            int id = int.Parse(Console.ReadLine());
            bool flag = list.ContainsKey(id);
            if (flag)
            {
                string name = list[id].ToString();
                Console.WriteLine("你想吃的品牌名称是：{0}",name);
            }
            else
            {
                Console.WriteLine("你想吃的牌子倒闭了");
            }
            Console.WriteLine("所有品牌的名称如下：");
            foreach(DictionaryEntry a in list)
            {
                int key = (int)a.Key;
                string value = a.Value.ToString();
                Console.WriteLine("编号：{0}，牌子：{1}",key,value);
            }
        }
    }
}
