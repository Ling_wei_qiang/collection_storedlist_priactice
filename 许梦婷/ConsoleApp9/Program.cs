﻿using System;
using System.Collections;

namespace ConsoleApp9
{
    class Program
    {
        static void Main(string[] args)
        {
            SortedList sortList = new SortedList();
            sortList.Add(1, "水浒传");
            sortList.Add(2, "红楼梦");
            sortList.Add(3, "西游记");
            Console.WriteLine("请输入图书编号：");
            int id = int.Parse(Console.ReadLine());
            bool flag = sortList.ContainsKey(id);
            if (flag)
            {
                string name = sortList[id].ToString();
                Console.WriteLine("您查找的图书名为：{0}", name);
            }
            else
            {
                Console.WriteLine("您查找的图书编号不存在！");
            }
            Console.WriteLine("所有的图书信息如下：");
            foreach (DictionaryEntry d in sortList)
            {
                int key = (int)d.Key; string value = d.Value.ToString();
                Console.WriteLine("图书编号：{0}，书名：{1}", key, value);
            }
        }
    }
}
