﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
namespace SortedListTest
{
    class Program
    {
        static void Main(string[] args)
        {
            SortedList sl = new SortedList();
            sl.Add(1, "鳗鱼饭");
            sl.Add(2, "酸菜鱼");
            sl.Add(3, "牛肉面");
            sl.Add(4, "兰州拉面");
            sl.Add(5, "水煮肉片");
            sl.Add(6, "老八秘制小汉堡");
            Console.WriteLine("欢迎来到深夜食堂！");
            Console.WriteLine("你想干嘛？1.吃饭 2.我就看看");
            while (true)
            {
                int index = int.Parse(Console.ReadLine());
                if (index == 1)
                {
                    Console.WriteLine("本店菜品信息如下：");
                    foreach (DictionaryEntry item in sl)
                    {
                        int key = (int)item.Key;
                        string name = item.Value.ToString();
                        Console.WriteLine("菜品编号为：{0}，菜品名为：{1}", key, name);
                    }
                    Console.WriteLine("请选择菜品编号：");
                    int id = int.Parse(Console.ReadLine());
                    bool flag = sl.ContainsKey(id);
                    if (flag)
                    {
                        string name = sl[id].ToString();
                        Console.WriteLine("您输入的编号为：{0}，其菜品是：{1}", id, name);
                        break;
                    }
                    else
                    {
                        Console.WriteLine("您输入的菜品编号有误");
                        Console.WriteLine("所有的菜品信息如下：");
                        foreach (DictionaryEntry item in sl)
                        {
                            int key = (int)item.Key;
                            string name = item.Value.ToString();
                            Console.WriteLine("菜品编号为：{0}，菜品名为：{1}", key, name);
                        }
                    }
                    break;
                }
                else if (index==2)
                {
                    Console.WriteLine("谢~谢光临！");
                    break;
                }
                else
                {
                    Console.WriteLine("您的输入有误！");
                    break;
                }
            }
        }
    }
}
