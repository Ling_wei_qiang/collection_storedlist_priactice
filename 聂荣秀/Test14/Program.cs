﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Test14
{
    class Program
    {
        static void Main(string[] args)
        {
            SortedList sort = new SortedList();
            sort.Add(002, "小葵");
            sort.Add(001, "大奎");
            sort.Add(003, "小贾");
            Console.WriteLine("请输入学生编号：");
            int id = int.Parse(Console.ReadLine());
            bool student = sort.ContainsKey(id);
            if (student )
            {
                string name = sort[id].ToString();
                Console.WriteLine("您查找的学生姓名为：{0}", name);
            }
            else
            {
                Console.WriteLine("您查找的学生编号不存在！");
            }
            Console.WriteLine("所有的学生信息如下：");
            foreach (DictionaryEntry d in sort)
            {
                int key = (int)d.Key;
                string value = d.Value.ToString();
                Console.WriteLine("学生编号：{0}，姓名：{1}", key, value);
            }
        }
    }
}
