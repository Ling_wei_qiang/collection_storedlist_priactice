﻿using System;
using System.Collections;

namespace ConsoleApp1
{
    class Program
    {
        static void Main(string[] args)
        {
            SortedList sortedlist = new SortedList();
            sortedlist.Add(1, "小明");
            sortedlist.Add(2, "小红");
            sortedlist.Add(3, "小胖");
            Console.WriteLine("请输入座位号：");
            int id = int.Parse(Console.ReadLine());
            bool di = sortedlist.ContainsKey(id );
            if (di)
            {
                string name = sortedlist[id].ToString();
                Console.WriteLine("你的名字是：{0}", name);
            }
            else
            {
                Console.WriteLine("名字不存在");
            }
            Console.WriteLine("信息表:");
            foreach (DictionaryEntry a in sortedlist)
            {
                int b = (int)a.Key;
                string c = a.Value.ToString();
                Console.WriteLine("你的座位号是：{0},你的名字是：{1}",b,c );
            }
        }
    }
}
