﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp2
{
    class Program
    {
        static void Main(string[] args)
        {
            SortedList sortedList = new SortedList();
            sortedList.Add(4, "西瓜");
            sortedList.Add(2, "香蕉");
            sortedList.Add(3, "苹果");
            sortedList.Add(1, "橘子");
            sortedList.Add(5, "芒果");
            sortedList.Add(6, "桃子");
            Console.WriteLine("已有水果编号");
            foreach (DictionaryEntry item in sortedList)
            {
                Console.WriteLine("{0}{1}",item.Key,item.Value);
            }
            Console.WriteLine();
            Console.WriteLine("请输入水果的编号：");
            int id = int.Parse(Console.ReadLine());
            var fruits = sortedList.ContainsKey(id);
            if (fruits)
            {
                Console.WriteLine("你选择的水果名字是：{0}",sortedList[id].ToString());
            }
            else
            {
                Console.WriteLine("没有找到这个水果！");
            }
        }
    }
}
