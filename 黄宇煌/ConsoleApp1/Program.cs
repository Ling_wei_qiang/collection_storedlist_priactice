﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1
{
    class Program
    {
        static void Main(string[] args)
        {
            //SortedList演示
            SortedList list = new SortedList();
            list.Add(1,"西游记");
            list.Add(3,"水浒传");
            list.Add(2,"红楼梦");
            //遍历打印所有书
            foreach(DictionaryEntry n in list)
            {
                Console.WriteLine("编号：{0} 书名：{1}",n.Key,n.Value);
            }
            
            //输入编号查询书
            Console.WriteLine("请输入你要查询的书的编号：");
            int id = int.Parse(Console.ReadLine());
            if (list.ContainsKey(id))
            {
                Console.WriteLine(list[id].ToString());
            }
            else
            {
                Console.WriteLine("小老弟，你的编号有问题吧！");
            }





        }
    }
}
